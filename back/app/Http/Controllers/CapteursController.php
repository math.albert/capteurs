<?php

namespace App\Http\Controllers;

use App\Capteurs;
use Illuminate\Http\Request;

//TODO Ajouter une methode show pour recuperer les valeurs entre deux dates
class CapteursController extends Controller
{
    
    public function showAllCapteurs()
    {
        return response()->json(Capteurs::all());
    }
    
    public function showOneCapteur($id)
    {
        return response()->json(Capteurs::find($id));
    }
    
    public function create(Request $request)
    {
        $capteur = Capteurs::create($request->all());
        
        return response()->json($capteur, 201);
    }
    
    public function update($id, Request $request)
    {
        $capteur = Capteurs::findOrFail($id);
        $capteur->update($request->all());
        
        return response()->json($capteur, 200);
    }
    
    public function delete($id)
    {
        Capteurs::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
