<?php

namespace App\Http\Controllers;

use App\Releves;
use Illuminate\Http\Request;

class RelevesController extends Controller
{
    
    public function showAllReleves()
    {
        return response()->json(Releves::all());
    }
    
    public function showRelevesOf($id_capteur)
    {
        return response()->json(Releves::where('id_capteur', '=', $id_capteur)->get());
    }
    
    public function create(Request $request)
    {
        $releve = Releves::create($request->all());
        
        return response()->json($releve, 201);
    }
    
    public function update($id, Request $request)
    {
        $releve = Releves::findOrFail($id);
        $releve->update($request->all());
        
        return response()->json($releve, 200);
    }
    
    public function delete($id)
    {
        Releves::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
