<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('capteurs',  ['uses' => 'CapteursController@showAllCapteurs']);
    $router->get('capteurs/{id}', ['uses' => 'CapteursController@showOneCapteur']);
    $router->post('capteurs', ['uses' => 'CapteursController@create']);
    $router->delete('capteurs/{id}', ['uses' => 'CapteursController@delete']);
    $router->put('capteurs/{id}', ['uses' => 'CapteursController@update']);

    $router->get('releves',  ['uses' => 'RelevesController@showAllReleves']);
    $router->get('releves/{id_capteur}', ['uses' => 'RelevesController@showRelevesOf']);
    $router->post('releves', ['uses' => 'RelevesController@create']);
    $router->delete('releves/{id}', ['uses' => 'RelevesController@delete']);
    $router->put('releves/{id}', ['uses' => 'RelevesController@update']);
});
